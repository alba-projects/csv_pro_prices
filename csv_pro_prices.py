#!/usr/bin/env python3

import sys
import os
import csv
import glob
import urllib.parse

################################################################################
# Internal functions

def try_open(filename):
    try:
        file = open(filename, "w+")
    except FileNotFoundError:
        os.makedirs(os.path.dirname(filename))
        file = open(filename, "w+")
    return file

def get_id(ugs, id_rows):
    for (id,p_ugs) in zip([r[0] for r in id_rows], [r[2] for r in id_rows]):
        if p_ugs == ugs:
            return id
    return "ID_KO"

def copy_and_rename(file, src_dir, dst_dir, is_dry_run):
    filename = os.path.basename(file)
    split_filename = filename.split("_")
    if len(split_filename) != 2:
        print("Error: image name (%s) shall have only 1 '_' (underscore) - Skip" % filename)
        return (-1, "")
    else:
        try:
            if not is_dry_run:
                os.makedirs(dst_dir + src_dir)
        except FileExistsError:
            pass
        if not is_dry_run:
            os.popen('cp "%s" %s' % (file, dst_dir + src_dir + split_filename[1]))
        return (split_filename[0], split_filename[1])

def add_image_in_csv(url_prefix, img_ugs, new_name, rows):
    ugs_list = [elt[1] for elt in rows]
    is_found = False
    for idx, ugs in enumerate(ugs_list):
        sub_parfum_ugs = -1
        is_ugs_ok = False
        if ugs == img_ugs:
            is_ugs_ok = True
        elif (ugs.find(img_ugs) == 0):
            try:
                sub_parfum_ugs = int(ugs[len(img_ugs):], base=10)
            except:
                print("Error: bad sub parfum ugs:{ugs}, img_ugs:{img_ugs} sub_parfum_ugs:{sub_ugs}"
                       .format(img_ugs=img_ugs, ugs=ugs, sub_ugs=ugs[len(img_ugs):]))
            is_ugs_ok = (sub_parfum_ugs in range(0,117))
        if is_ugs_ok:
            extra_coma = "" if len(rows[idx][5]) == 0 else ", "
            rows[idx][5] += extra_coma + url_prefix + urllib.parse.quote(new_name)
            is_found = True

    if not is_found:
        print("Error: ugs {img_ugs} not found in CSV for image {name}".format(img_ugs=img_ugs,
                                                                          name=new_name))
    return rows

def get_srv_subpath(new_name, srv_list):
    for srv_img in srv_list:
        if new_name in srv_img:
            #ret = ... + srv_img.replace(new_name,"").split("uploads")[0]
            ret = "wp-content/cache/tmp/" + srv_img.replace(new_name,"").split("uploads")[0]
            return ret
    return "wp-content/cache/tmp/"

################################################################################
# A) ID = 0
# B) UGS = 1
# C) Name = 2
# D) Parent = 3
# E) Type = 4
# F) Image = 5
# G) DESCRIPTION = 6
# H) DESCRIPTION COURTE = 7
# I) Published = 8
# J) Position = 9
# K) Regular Price = 10
# L) Sale Price = 11
# M) Attribute 1 name =  12
# N) Attribute 1 value(s) = 13
# O) Attribute 1 visible = 14
# P) Attribute 1 global = 15
# Q) Attribute 2 name = 16
# R) Attribute 2 value(s) = 17
# S) Attribute 2 visible = 18
# T) Attribute 2 global = 19
# U) Attribute 3 name = 20
# V) Attribute 3 value(s) = 21
# X) DISABLED Attribute 3 visible = 22
# Y) DISABLED Attribute 3 global = 23
# Z) Tax status = 24
# AZ) Tax class = 25
# AB) In stock? = 26
# AC) STOCK = 27
# AD) Backorders allowed? = 28
# AE) Longueur (cm) = 29
# AF) Hauteur (cm) = 30
# AG) Largeur (cm) = 31
# AH) Poids (kg) = 32
# AI) Allow customer reviews? = 33
# AJ) CATEGORIES = 34
# AK) Tags = 35
# AL) Meta: _wpcom_is_markdown = 36


def main():
    # 0) Parse argument
    user_role = sys.argv[1]
    in_filename = sys.argv[2]
    id_filename = sys.argv[3] if len(sys.argv) > 3 else in_filename
    taxe_coef = sys.argv[4] if len(sys.argv) > 4 else 1.2
    dst_dir = sys.argv[5] if len(sys.argv) > 5 else "out"

    if dst_dir[-1] != '/':
        dst_dir += "/"

    # 1) Local variable declaration
    out_filename = dst_dir + os.path.basename(in_filename.replace(".csv", "_B2B.csv"))
    out_header = ["Product ID",
                  "SKU",
                  "Product Name",
                  "Rôle de l'utilisateur",
                  "Courriel du client",
                  "Quantity From(min qty)",
                  "Quantity To(max qty)",
                  "Adjustment/Discount Type",
                  "Discount Price/Value",
                  "Replace Original Price"]

    # 2) Core processing

    # Open the files
    in_file = open(in_filename)
    id_file = open(id_filename)
    out_file = try_open(out_filename)
    # Convert it as csv object
    in_csvreader = csv.reader(in_file)
    id_csvreader = csv.reader(id_file)
    # Convert it as csv writter
    csvwriter = csv.writer(out_file)
    # Squize in_file and in_file header
    next(in_csvreader)
    next(id_csvreader)
    csvwriter.writerow(out_header)
    # Get others row (data)
    in_rows = [row for row in in_csvreader]
    id_rows = [row for row in id_csvreader]
    # Close reader
    in_file.close()
    id_file.close()

    # Get informations for out file generation by USG
    ugs_array =  [r[1] for r in in_rows]
    name_array =  [r[2] for r in in_rows]
    type_array =  [r[4] for r in in_rows]
    pro_price_array =  [r[11] for r in in_rows]
    data_array = []
    for line, ugs in enumerate(ugs_array):
        if type_array[line].lower() != "variable":
            p_id = get_id(ugs, id_rows)
            try:
                p_price = float(pro_price_array[line].split(" ")[0].replace(",", "."))
                p_price = round(p_price * taxe_coef, 2) # TTC operation
            except:
                print('Unexpected price that is not a number: %s (ugs: %s, name: %s)' % (pro_price_array[line], ugs, name_array[line]))
                p_price = 424242
            data = [p_id, # Product ID
                    ugs, # SKU
                    name_array[line], # Product Name
                    user_role,# Rôle de l'utilisateur
                    "", # Courriel du client
                    "", # Quantity From(min qty)
                    "", # Quantity To(max qty)
                    "fixed_price", # Adjustment/Discount Type
                    p_price, #Discount Price/Value
                    "yes"] #Replace Original Price
            csvwriter.writerow(data)
        elif len(pro_price_array[line]) != 0:
            print('Unexpected Variable (ugs: %s, name: %s ) with price (%s)' % (ugs, name_array[line], pro_price_array[line]))
    # Close the file
    out_file.close()

    print("Done")

main()
